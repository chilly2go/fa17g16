/* Wir haben 5 Bilder für unsere 5 Testhäuser */
INSERT INTO fa17g16.real_estate_medias (`id`, `type`, `path`, `estate_id`) VALUES
(1,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus1.jpg",1),
(31,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus1.jpg",1),
(32,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus1.jpg",1),

(2,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus2.jpg",2),
(33,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus2.jpg",2),
(34,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus2.jpg",2),

(3,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus3.jpg",3),
(35,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus3.jpg",3),
(36,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus3.jpg",3),

(4,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus4.jpg",4),
(37,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus4.jpg",4),
(38,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus4.jpg",4),

(5,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus5.jpg",5),
(39,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus5.jpg",5),
(40,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus5.jpg",5),

(6,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus6.jpg",6),
(41,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus6.jpg",6),
(42,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus6.jpg",6),

(7,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus7.jpg",7),
(43,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus7.jpg",7),
(44,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus7.jpg",7),

(8,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus8.jpg",8),
(45,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus8.jpg",8),
(46,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus8.jpg",8),

(9,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus9.jpg",9),
(47,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus9.jpg",9),
(48,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus9.jpg",9),

(10,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus10.jpg",10),
(49,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus10.jpg",10),
(50,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus10.jpg",10),

(11,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus11.jpg",11),
(51,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus11.jpg",11),
(52,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus11.jpg",11),

(12,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus12.jpg",12),
(53,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus12.jpg",12),
(54,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus12.jpg",12),

(13,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus13.jpg",13),
(55,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus13.jpg",13),
(56,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus13.jpg",13),

(14,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus14.jpg",14),
(57,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus14.jpg",14),
(58,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus14.jpg",14),

(15,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus15.jpg",15),
(59,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus15.jpg",15),
(60,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus15.jpg",15),

(16,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus16.jpg",16),
(61,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus16.jpg",16),
(62,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus16.jpg",16),

(17,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus17.jpg",17),
(63,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus17.jpg",17),
(64,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus17.jpg",17),

(18,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus18.jpg",18),
(65,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus18.jpg",18),
(66,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus18.jpg",18),

(19,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus19.jpg",19),
(67,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus19.jpg",19),
(68,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus19.jpg",19),

(20,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus20.jpg",20),
(69,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus20.jpg",20),
(70,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus20.jpg",20),

(21,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus21.jpg",21),
(71,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus21.jpg",21),
(72,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus21.jpg",21),

(22,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus22.jpg",22),
(73,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus22.jpg",22),
(74,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus22.jpg",22),

(23,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus23.jpg",23),
(75,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus23.jpg",23),
(76,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus23.jpg",23),

(24,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus24.jpg",24),
(77,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus24.jpg",24),
(78,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus24.jpg",24),

(25,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus25.jpg",25),
(79,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus25.jpg",25),
(80,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus25.jpg",25),

(26,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus26.jpg",26),
(81,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus26.jpg",26),
(82,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus26.jpg",26),

(27,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus27.jpg",27),
(83,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus27.jpg",27),
(84,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus27.jpg",27),

(28,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus28.jpg",28),
(85,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus28.jpg",28),
(86,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus28.jpg",28),

(29,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus29.jpg",29),
(87,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus29.jpg",29),
(88,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus29.jpg",29),

(30,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus30.jpg",30),
(89,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus30.jpg",30),
(90,"picture","https://sfsuse.com/fa17g16/public/images/houses/smallsize/Haus30.jpg",30);



